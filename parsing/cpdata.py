#! /usr/bin/env python
import re
import os
import collections
import abc
# import numpy as np
# from .iotk import etree
from .. physics import atoms

'''
Step Time Separator Pattern

step1 time1
 ...
Records
 ...
stepN timeN
 ...
Records
 ...

Examples: *.pos, *vel, *for, *cel, *spr
'''
_STEP_TIME_SEP = [r'^\s*([^\s]*)\s+([^\s]*)\s*$']

# =---------------------------------------------------------------------------= #
# Base Class
# =---------------------------------------------------------------------------= #
class _DataFile(metaclass=abc.ABCMeta):
    """
    Base class for Quantum-Espresso data files

    Data is stored in the general format:
        record separator1
        ...
        record separatorN
        Data Line 1
        ...
        Data Line M
    """

    sizelimit = 100.0 # MB

    # File Extension (defined in subclasses)
    _ext = None

    # Record Seperator Patterns
    _record_sep = []

    # Skip regex pattern
    _skip_pattern = '(^\s+$|^#+)'

    def __init__(self, filename, skip):
        self._filename = filename
        self._skip = skip

        # Internally stored variables (size limited)
        self._lines = []
        self._data = collections.OrderedDict()

        # Looping variables (for generator and data)
        self._curline = None
        self._seperator = []
        self._record_lines = []
        self._record_data = None
        self._nline = 0

        # Create array of re pattern objects for each line in self.record_sep
        self._patterns = []
        for string in self._record_sep:
            self._patterns.append(re.compile(string))

        self._check_file_exists()

        self._check_file_extension()

    @property
    def filename(self):
        ''' Name of the data file, should not be changed '''
        return self._filename

    @property
    def filesize(self):
        ''' Size of the data file in MB '''
        statinfo = os.stat(self.filename)
        size = statinfo.st_size / 10.0 ** 6
        return size

    def _check_file_exists(self):
        ''' Check that the file exists '''
        if not os.path.isfile(self.filename):
            raise OSError("File Not Found: {}".format(self.filename))

    def _check_file_extension(self):
        temp, ext = os.path.splitext(self.filename)
        if ext != self._ext:
            raise DataFileError('Class {} expects an extension of {}, found {}'.format(
                self.__class__.__name__, self.__class__._ext, ext))

    def _check_size_ok(self):
        ''' Check the size against the class variable sizelimit '''
        if self.filesize > self.__class__.sizelimit:
            raise FileToLargeError(self.filename)

    @property
    def lines(self):
        '''
            Read all of the data file into a list of strings: self.lines
            skip = True will remove all blank and commented lines
        '''
        if not self._lines:
            self._check_size_ok()
            with open(self.filename) as f:
                if self._skip:
                    self._lines = [line.strip() for line in
                                                    f if not re.search(self._skip_pattern, line)]
                else:
                    self._lines = [line.strip() for line in f]
        return self._lines

    @property
    def data(self):
        ''' Return structured data '''
        if not self._data:
            self._check_size_ok()
            self._curline = None
            self._nline = 0
            with open(self.filename, 'r') as self.fobj:
                while True:
                    try:
                        self._collect_sep_record()
                    except EOFError:
                        break
                    self._update_data()
        return self._data

    def __iter__(self):
        ''' Iterate through a strucutred '''
        self._curline = None
        self._nline = 0
        with open(self.filename, 'r') as self.fobj:
            while True:
                try:
                    self._collect_sep_record()
                except EOFError:
                    break
                yield self._yield_data()

    def _readline(self):
        '''
        Read the current self.fileobj, stores line and line number as
        self._curline and self._nline (respectively) for future use
        '''
        while True:
            line = self.fobj.readline()
            self._nline += 1

            # Check for skip patterns
            if self._skip and re.search(self._skip_pattern, line):
                continue
            else:
                break
        self._curline = line

    def _collect_sep_record(self):
        '''Collect the record and separator, call record parser'''
        # Setup NEW record and separator lines
        self._record_lines = []
        self._seperator = []

        # === 1) Collect the separator ===
        # First read of file, subsequent loops start from last read
        if self._curline is None:
            self._readline()

        # Check for all separator patterns, add to separator
        for pattern in self._patterns:
            m = pattern.search(self._curline)
            assert m, "Separator pattern not found at assumed position"
            for group in m.groups():
                self._seperator.append(group)
            self._readline()

        # === 2) Collect the Record ===
        while True:

            # End of file
            if not self._curline:
                self._parse_record()
                raise EOFError

            # Found the next record separator
            if re.search(self._patterns[0], self._curline):
                self._parse_record()
                break
            else:
                self._record_lines.append(self._curline)
            self._readline()

    @abc.abstractmethod
    def _parse_record(self):
        ''' Parse the self._record_lines and add them to self._record_data'''
        pass

    @abc.abstractmethod
    def _update_data(self):
        ''' Package self._record_data into self._data'''
        pass

    @abc.abstractmethod
    def _yield_data(self):
        ''' Yield information from self._record_data while using __iter__'''
        pass
# =---------------------------------------------------------------------------= #

# =---------------------------------------------------------------------------= #
#  Second Level Base Classes
# =---------------------------------------------------------------------------= #
class _Atomic_XYZ_Files(_DataFile):

    _atoms_attribute = None

    _record_sep = _STEP_TIME_SEP

    def __init__(self, filename, species_symbols, species_numbers, skip=True):
        if len(species_symbols) != len(species_numbers):
            raise DataFileError('Must specify species_symbols, and species_numbers')
        else:
            self.species = collections.OrderedDict()
            for symbol, number in zip(species_symbols, species_numbers):
                self.species.update({symbol: number})
        super().__init__(filename, skip)

    def _parse_record(self):
        '''Uses self._record_lines to create self._record_data'''
        # Populate a temporary OrderedDict to be placed into self._record_data
        tempdict = collections.OrderedDict()
        for specie in self.species.keys():
            tempdict.update({specie: []})
            for num in range(self.species[specie]):
                atom = [float(i) for i in self._record_lines.pop(0).split()]
                tempdict[specie].append(atom)

        # Instantiate an AtomSpecies object and corresponding set _atoms_attribute
        self._record_data = atoms.AtomSpecies(*list(self.species.keys()))
        for specie in tempdict.keys():
            setattr(self._record_data[specie], self._atoms_attribute, tempdict[specie])

    def _update_data(self):
        '''Update self._data'''
        step, time = self._seperator
        self._data.update({int(step): self._record_data})

    def _yield_data(self):
        '''Yield data'''
        step, time = self._seperator
        return int(step), float(time), self._record_data

# =---------------------------------------------------------------------------= #

# =---------------------------------------------------------------------------= #
#  Public API
# =---------------------------------------------------------------------------= #
class PosFile(_Atomic_XYZ_Files):

    _ext = '.pos'

    _atoms_attribute = 'positions'

    def __init__(self, filename, species_symbols, species_numbers, skip=True):
        super().__init__(filename, species_symbols, species_numbers, skip)


def merge_iter(*args):
    ''' Merge iterators of _Atomic_XYZ_Files sub-classed objects '''
    pass


def merge_data(*args):
    ''' Merge data of _Atomic_XYZ_Files sub-classed objects '''
    pass

# =---------------------------------------------------------------------------=#
#  Exceptions Classes
# =---------------------------------------------------------------------------=#
class DataFileError(Exception):
    pass

class FileToLargeError(Exception):
    def __init__(self, obj):
        msg = "Error: file '{}' is larger then the {} MB size limit"
        msg = msg.format(obj.filename, obj.sizelimit)
        super().__init__(msg)

class IncorrectFormatError(Exception):
    def __init__(self, filename, reason):
        msg = "Error: file '{}' is incorrectly formatted. {}".format(filename, reason)
        super().__init__(msg)
