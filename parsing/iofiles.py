
import os
import re
import collections
import ast
import abc
# from ..physics import atoms

POS = 'ATOMIC_POSITIONS'
SYS = 'SYSTEM'

class QEFile(metaclass=abc.ABCMeta):

    def __init__(self, value=None, fromfile=False, noparse=False):
        self._set_init_file(value, fromfile, noparse)

    def _set_init_file(self, value, fromfile=False, noparse=False):
        '''
        Read the initial file
        Three Options:
            1) a fileobject
            2) a filename (must set fromfile=True)
            3) a string (useful for creating an empty file')
        '''
        if hasattr(value, 'read'):
            # file-like object
            init_string = value.read()
        elif fromfile:
            # file name
            self.filename = value
            init_string = open(self.filename, 'r').read()
        else:
            # file string
            if value is None:
                init_string = ''
            else:
                init_string = value
        if not noparse:
            self._parse_file(init_string)

    @abc.abstractmethod
    def _parse_file(self, init_string):
        pass

    @abc.abstractmethod
    def write(self, filename=None):
        pass

# TODO
# 1) Add .true. and .false. conversions to namelist_set and namelist_get
class InputFile(QEFile):

    # Start Node Patterns
    _start_patterns = {
        'namelist' : re.compile(r'^\s*\&([a-zA-Z_]+)\s*$'),
        'cards' : re.compile(r'^\s*([a-zA-z_]+)\s*({)?([a-zA-Z_]*)(})?\s*$')
    }
    # End Node Patterns. Card ending is a suggested ending, the true determining
    # factor that a card has ended is that a new namelist or card has began.
    _end_patterns = {
        'namelist' : re.compile('^\s*\/\s*$'),
        'cards' : re.compile(r'^\s*$')
    }
    # Namelist, variable and value patterns
    _namelist_variable_pattern = re.compile(
        '^\s*([a-z_+\-\.\d/\(\)]+)\s*=\s*(\'|")?([a-z_+\-\.\d/]+)(\'|")?\s*,?\s*$',
        flags=re.MULTILINE | re.IGNORECASE
    )

    def __init__(self, invalue=None, fromfile=False, **kwargs):
        self._setup_main_data()
        super().__init__(invalue, fromfile, **kwargs)

        self.__atomic_positions = None
        self.__atomic_velocities = None
        self.__atomic_forces = None

    def _setup_main_data(self):
        # Main data for input file
        # Note: Namelist variables are stored internally with quotes,
        # but are removed when calling for a variables value with
        # either namelist_get(), namelist_values(), or namelist_items()
        self._namelists_data = collections.OrderedDict()
        self._cards_data = collections.OrderedDict()
        self._cards_mod_data = collections.OrderedDict()
        self.unknown_lines = []

    def _parse_file(self, init_string):
        '''
        check current line for start patterns (namelist, cards, ...),
        pass the remaining lines along, with match object, to the
        apropreiate parser, whicih will find the current node and
        return the remaining lines.
        '''
        # clear all previous data
        self._setup_main_data()

        lines = init_string.split('\n')
        while lines:
            current_line = lines.pop(0)
            # Search for start pattern
            for name, pattern in self._start_patterns.items():
                match = pattern.search(current_line)
                if match:
                    lines = self._parse_node(lines, match, name)
                    # only one start pattern per line
                    break
            else:
                self.unknown_lines.append(current_line)

    def _parse_node(self, remaining_lines, match, type_of_node):
        '''
        Parse the current node, return remaining_lines.
        Uses a match object, and the name of the current node.
        unknown lines added to self.unknown_lines
        '''

        # Node tag name (nmaelist or card)
        tag = match.group(1).upper()

        # Update the data dicts
        if type_of_node == 'namelist':
            self._namelists_data.update({tag: collections.OrderedDict()})
        elif type_of_node == 'cards':
            self._cards_data.update({tag: []})
            # Card modifier, same line as tag name
            if match.group(3):
                self._cards_mod_data.update({tag: match.group(3)})
            else:
                self._cards_mod_data.update({tag: ''})

        # Loop through remaining lines
        while True:
            try:
                current_line = remaining_lines.pop(0)
            except IndexError:
                break

            # Break if end pattern is found
            if self._end_patterns[type_of_node].search(current_line):
                break

            # Individual Nodes
            # 1) Namelist
            if type_of_node == 'namelist':
                # Make sure namelist was not interrupted by another namelist/card
                for name, pattern in self._start_patterns.items():
                    if pattern.search(current_line):
                        raise InputFileError('Namelist {} was interrupted'.format(tag))

                m = self._namelist_variable_pattern.search(current_line)
                if m:
                    variable = m.group(1).strip().lower()
                    value = m.group(3).strip().lower()

                    # IMPORTANT: Quotes are preserved in namelist data!!
                    # Unmatched groups could be None
                    quotes = set([m.group(2), m.group(4)])
                    if len(quotes) == 1:
                        quote = quotes.pop()
                        if quote:
                            value = "".join([quote, value, quote])
                    else:
                        msg = "errant, or unclosed quotes in {}".format(variable)
                        raise InputFileError(msg)

                    self._namelists_data[tag].update({variable : value})

                else:
                    self.unknown_lines.append(current_line)

                continue

            # 2) Cards
            elif type_of_node == 'cards':
                # Because there is no *hard* ending to a card node (only the *suggested*
                # new line) there is no way to determine if this a *correct* ending or
                # an erroneous interruption without analyzing the actual data (which
                # would make this very fragile). Therefore we will add a second break
                # point (in addition to the end_pattern search above) if a new
                # start_pattern is found.
                try:
                    for name, pattern in self._start_patterns.items():
                        if pattern.search(current_line):
                            raise InputFileError()
                except InputFileError:
                    break

                self._cards_data[tag].append(current_line.strip())

                continue

        return remaining_lines

    @property
    def namelists(self):
        ''' '''
        return list(self._namelists_data.keys())

    @property
    def cards(self):
        ''' '''
        return list(self._cards_data.keys())

    def namelist_keys(self, namelist):
        ''' Return namelist keys, variable names '''
        namelist = namelist.upper()
        try:
            return list(self._namelists_data[namelist].keys())
        except KeyError as ierr:
            raise InputFileError("Namelist '{}' Does Not Exist".format(ierr))

    def namelist_values(self, namelist):
        ''' Return namelist values, variable names '''
        namelist = namelist.upper()
        try:
            return [i.strip("'\"") for i in self._namelists_data[namelist].values()]
        except KeyError as ierr:
            raise InputFileError("Namelist '{}' Does Not Exist".format(ierr))

    def namelist_items(self, namelist):
        ''' Return namelist items, (variable_names, values) '''
        namelist = namelist.upper()
        try:
            return [(k.strip("'\'"), v.strip("'\'")) for k, v in
                                        self._namelists_data[namelist].items()]
        except KeyError as ierr:
            msg = ' {} Does Not Exist'.format(ierr)
            if ierr == namelist:
                msg = 'Namelist' + msg
            else:
                msg = 'Variable' + msg
            raise InputFileError(msg)

    def namelist_get(self, namelist, variable):
        ''' Get a particular variable's value from a particular namelist '''
        namelist = namelist.upper()
        try:
            return self._namelists_data[namelist][variable].strip("'\"")
        except KeyError as ierr:
            msg = ' {} Does Not Exist'.format(ierr)
            if ierr == namelist:
                msg = 'Namelist' + msg
            else:
                msg = 'Variable' + msg
            raise InputFileError(msg)

    def namelist_set(self, namelist, variable, value, noquotes=False):
        '''
        Set a particular variable's value from a particular namelist.
        Value will be run through ast.literal_eval to check for numbers
        if no numbers are found then it will (by default) considered it
        a string. When strings are written to a Fortran namelist they
        must be enclosed in quotes.

        @namelist, namelist name
        @variable, variable inside  namelist
        @value, value of variable
        @noquotes, will not include quotes even if it fails the ast.literal_eval
            test (ex: 123.0D0 a common occurrence in Fortran programs)
        '''
        if namelist not in self._namelists_data.keys():
            self._namelists_data.update({namelist.upper(): collections.OrderedDict()})
        try:
            value = ast.literal_eval(value)
        except (ValueError, SyntaxError):
            if not noquotes:
                value = ''.join(["'", value, "'"])
        self._namelists_data[namelist][variable] = value

    def card_get(self, card):
        ''' Get named card's values '''
        try:
            return list(self._cards_data[card])
        except KeyError:
            raise InputFileError("Card '{}' Not Found".format(card))

    def card_set(self, card, replace):
        '''
        Set named card's values

        value = if not list or typle it will be converted
        '''
        if not isinstance(replace, (list, tuple)):
            replace = [replace]

        if card not in self._cards_data.keys():
            self._cards_data.update({card.upper(): collections.OrderedDict()})
        self._cards_data[card] = replace

    def card_mod_get(self, card):
        '''
        Get a Cards Modifier

        EX: ATOMIC_POSITIONS {bohr}
        '''
        return self._cards_mod_data[card]

    def card_mod_set(self, card, replace):
        '''
        Get a Cards Modifier

        EX: ATOMIC_POSITIONS {bohr}
        '''
        if card not in self._cards_mod_data.keys():
            self._cards_mod_data.update({card.upper(): collections.OrderedDict()})
        self._cards_mod_data[card] = replace

    def write(self, filename=None):
        '''
        Write output as a single string.

        As it stands, if any letters are present in a variable
        value, it will be enclosed in qutoes
        '''
        output = ''

        # Namelists
        for namelist in self.namelists:
            output += '\n&{}'.format(namelist)
            # Cannot use self.namelist_items because it removes quotes
            for name, value in self._namelists_data[namelist].items():
                output += '\n   {:20}  = {:<}'.format(name, value)
            output += '\n/'

        # Cards
        for card in self.cards:
            if self.card_mod_get(card):
                output += '\n\n{}  {}'.format(card, self.card_mod_get(card))
            else:
                output += '\n\n{}'.format(card)
            for line in self.card_get(card):
                output += '\n{}'.format(line)

        if filename:
            open(filename, 'w').write(output)
        else:
            return output

# FIXME  -> atoms.py has been changed
#    @property
#    def atom_pos(self):
#        if not self.__atomic_positions:
#            self.__atomic_positions = \
#                    atoms.AtomPositions(units=self._cards_mod_data[POS], *self._cards_data[POS])
#        return self.__atomic_positions
#
#    @atom_pos.setter
#    def atom_pos(self, value):
#        self.__atomic_positions = value
#        newvalue = []
#        for label, ats in self.__atomic_positions:
#            for at in ats:
#                newvalue.append(label + '  '.join([str(i) for i in at]))
#        self._cards_data[POS] = newvalue
#        self._cards_mod_data[POS] = self.__atomic_positions.units
#
#    @property
#    def atom_vel(self):
#        pass
#
#    @atom_vel.setter
#    def atoms_vel(self, value):
#        pass
#
#    @property
#    def atom_force(self):
#        pass
#
#    @atom_force.setter
#    def atoms_force(self, value):
#        pass
#
#    @property
#    def lat_vecs(self):
#        pass
#
#    @lat_vecs.setter
#    def lat_vecs(self, value):
#        pass

class OutputFile(QEFile):

    def __init__(self, outvalue=None, fromfile=False, **kwargs):
        self.calc_type = ''
        super().__init__(outvalue, fromfile, **kwargs)

    def _parse_file(self, init_string):
        _calc_mapping = {
            'PWSCF': self._parse_PWSCF,
            'CP': self._parse_CP}

        # Internal record of the output file
        self._raw_string = init_string

        # Values that should *always* be determined below
        self.complete = bool(re.search('JOB DONE.', self._raw_string))
        self.prefix = ''
        self.toteng = ''
        self.toteng_units = ''

        # Check the calculation type
        m = re.search('^\s*Program\s+([A-Z]+)', self._raw_string)
        if m:
            self.calc_type = m.group(1)
            try:
                _calc_mapping[self.calc_type]()
            except KeyError:
                pass

        # Get execution times (Assumes that all allowed types print the same info)
        if self.calc_type and self.complete:
            pattern = '{}\s*:\s*([0-9\.]+[smdh])\s+CPU\s*([0-9\.]+[smdh])\s+WALL'
            m = re.search(pattern.format(self.calc_type), self._raw_string)
            if m:
                self.walltime = self._convert_time(m.group(1))
                self.cputime = self._convert_time(m.group(2))

    def _convert_time(self, string):
        time_map = {
            's': 1,
            'm': 60,
            'h': 3600,
            'd': 86400}

        unit = string[-1]
        return float(string[:-1]) * time_map[unit]

    def _parse_CP(self):
        ''' Parse the CP File'''
        m = re.search('writing restart file:\s*([\_\.\/\-a-zA-Z0-9]+)_([0-9]+)\.save', self._raw_string)
        if m:
            # CP does prepend the outdir
            self.outdir, self.prefix = os.path.split(m.group(1).strip('_'))
            self.save_num = m.group(2)
            self._toteng_CP()

    def _toteng_CP(self):
        # FIXME -> Is there a better way to do this?
        """
        Find the Total Energy from a CP calculation output AND prefix.evp
        in Hartree a.u.. Because of the format of CP files there is no
        definitive final energies. Therefore we will have to read the 5th
        columns of the last line of prefix.evp and assume that this is the
        correct value.
        Assumes that the 5th column of the *.evp file is the total energy
        """
        _evp_toteng_column = 4

        try:
            evp_file = os.path.join(self.outdir, self.prefix + '.evp')
            evp_lines = open(evp_file).readlines()
            self.toteng = float(evp_lines[-1].split()[_evp_toteng_column])
            self.toteng_units = 'Hartree a.u.'
        except IOError:
            return None

    def _parse_PWSCF(self):
        ''' Parse the PW File'''
        m = re.search('Writing output data file\s*([\._\-\/a-zA-Z0-9]+)\.save', self._raw_string)
        if m:
            # PWSCF does *NOT* prepend the outdir
            self.prefix = m.group(1)
        self._toteng_PWSCF()

    def _toteng_PWSCF(self):
        """
        Find the Total Energy from a PW calculation output in Ry.
        Always found on the line with a '!'
        """
        try:
            m = re.search(
                '\s*!\s*total\s*energy\s*=\s*([\d+\-\.eE]+) Ry',
                self._raw_string,
                flags=re.MULTILINE
            )
            self.toteng = float(m.group(1))
            self.toteng_units = 'Ry'
        except AttributeError:
            return None

    def match(self, pattern):
        ''' Return the groups of an re module match '''
        m = re.search(pattern, self._raw_string, flags=re.MULTILINE)
        try:
            return m
        except AttributeError:
            return None

    def write(self, filename):
        if filename:
            open(filename, 'w').write(self._raw_string)
        else:
            return self._raw_string


# Exceptions =============================

class InputFileError(Exception):
    pass

class OutputFileError(Exception):
    pass
