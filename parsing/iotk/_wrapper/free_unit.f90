subroutine free_unit(iunit, ierr)
   !
   use iotk_module
   !
   implicit none
   !
   integer,          intent(out) :: iunit
   integer,          intent(out) :: ierr
   ! 
   !Temp Output arrays
   integer                    :: iunit_
   integer                    :: ierr_
   !
   !Call Routine
   call iotk_free_unit(unit=iunit_, ierr=ierr_)
   !
   !Assign Output varables
   iunit = iunit_
   ierr = ierr_
   !
end subroutine
