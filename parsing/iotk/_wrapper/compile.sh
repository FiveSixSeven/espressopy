# Makeshift compliation
#
#gfortran -O3 -g -x f95-cpp-input -D__GFORTRAN -D__STD_F95 -D__FFTW3 -I../S3DE/iotk/src/ -c fft.f90
#gfortran -O3 -g -x f95-cpp-input -D__GFORTRAN -D__STD_F95 -D__FFTW3 -I../S3DE/iotk/src/ -c read-bin.f90
##gfortran fft.o read-bin.o -L../S3DE/iotk/src -liotk 
#gfortran fft.o read-bin.o ../S3DE/iotk/src/libiotk.a -lfftw3 -lblas -llapack
#
#
IOTKDIR=/home/charles/Programming/Software-Packages/espresso-5.1-IOTK/S3DE/iotk/src/
SOURCES='free_unit.f90 open_read.f90 close_read.f90 scan_begin.f90 scan_end.f90 scan_empty.f90 scan_dat.f90 '
f2py3 -m _libiotk -h sgnFile.pyf $SOURCES  --overwrite-signature
f2py3 -c --fcompiler=gnu95 sgnFile.pyf $SOURCES -I$IOTKDIR -L$IOTKDIR -liotk
