subroutine open_read(iunit, filename, binary, raw, attr, ierr, root)
   !
   use iotk_module
   !
   implicit none
   !
   !Main IN/OUT Variables
   integer,                   intent(in)              :: iunit
   character(len=256),        intent(in)              :: filename
   logical,                   intent(in), optional    :: binary
   logical,                   intent(in), optional    :: raw
   !
   integer,                   intent(out)             :: ierr
   character(len=256),        intent(out)             :: attr
   character(len=256),        intent(out)             :: root
   !
   !Temp Input variables variables
   character(len=256)         :: filename_
   logical                    :: binary_
   logical                    :: raw_
   !
   !Temp Output variables
   integer                    :: iunit_
   integer                    :: ierr_
   character(len=256)         :: attr_
   character(len=256)         :: root_
   !
   !Assign Input defaults
   filename_ = filename
   iunit_ = iunit
   !
   if (.not. present(binary)) then
      binary_ = binary
   else
      binary_ = .false.
   endif
   !
   if (.not. present(raw)) then
      raw_ = raw
   else
      raw_ = .false.
   endif
   !
   !Call Routine
   call iotk_open_read(iunit_, file=trim(filename_), attr=attr_, binary=binary_, raw=raw_, root=root_, ierr=ierr_)
   !
   !Assign Output variables
   ierr = ierr_
   attr = attr_
   root = root_
   !
end subroutine open_read
