#! /usr/bin/python

import inspect
import ast
from . import _wrapper

def decode(value):
    try:
        value = value.decode('ascii')
    except AttributeError:
        pass
    return value

def free_unit():
    iunit, ierr = _wrapper._free_unit()
    # Fortran ierr, 0 is 'OK'
    if ierr == 0:
        return iunit

def open_read(filename, iunit=None, binary=False, raw=False):
    if not iunit:
        iunit = free_unit()
    elif not isinstance(iunit, int):
        raise ScanError('iunit must be an integer (IOTK requirement)')
    attr, ierr, root = _wrapper._open_read(iunit, filename, binary, raw)
    # Fortran ierr, 0 is 'OK'
    if ierr != 0:
        raise ScanError('Error in {} opening file {}'.format(inspect.stack()[0][3], filename))
    return iunit, convert_attr(decode(attr)), decode(root).strip()


def close_read(iunit):
    ierr = _wrapper._close_read(iunit)
    # Fortran ierr, 0 is 'OK'
    if ierr != 0:
        raise ScanError('Error in {} closing file with fortran file unit {}'.format(inspect.stack()[0][3], iunit))


def scan_begin(iunit, name):
    attr, found, ierr = _wrapper._scan_begin(iunit, name)
    # Fortran ierr, 0 is 'OK'
    if ierr != 0:
        raise ScanError('Error in {} beginning tag {}'.format(inspect.stack()[0][3], name))
    return bool(found), convert_attr(decode(attr))


def scan_end(iunit, name):
    ierr = _wrapper._scan_end(iunit, name)
    # Fortran ierr, 0 is 'OK'
    if ierr != 0:
        raise ScanError('Error in {} beginning tag {}'.format(inspect.stack()[0][3], name))


def scan_empty(iunit, name):
    attr, found, ierr = _wrapper._scan_empty(iunit, name)
    if ierr != 0:
        raise ScanError('Error in {} scanning empty tag {}'.format(inspect.stack()[0][3], name))
    return bool(found), convert_attr(decode(attr))


def scan_dat(iunit, name, convert=True):
    """
    Read the data (or text) within an open and close tag

    <name ... type='...' [columns='...'] size='...'>
    ...
    Data
    ...
    </name>

    Inputs:
    -------
        iunit       = Fortran unit number
        name        = Name of the tag
        convert     = (optional) convert to correct dimension (columns, size/columns)

    Outputs:
    -------
        data        = single value or number array numpy array of the data

    Data is initially returned as a single dimensional array but converted
    to an array (iotk uses a C-style ROW convention) by default (convert)
    """
    # Dictionary of all Fortran types call functions (i = iunit, n = name, s = size)
    calldict = {
        'integer' : lambda i, n, s : _wrapper._scan_dat.read_integer(i, n, s),
        'real' : lambda i, n, s : _wrapper._scan_dat.read_real(i, n, s),
        'complex' : lambda i, n, s : _wrapper._scan_dat.read_complex(i, n, s),
    }
    size, columns, type = _wrapper._scan_dat.gettag(iunit, name)
    type = decode(type).strip()

    # Call correct scan-dat function
    if type:
        try:
            retval = calldict[type](iunit, name, size)
        except KeyError:
            raise ScanError('Unknown type {} found, perhaps not implemented yet?')
        # Convert to the correct array shape
        if convert:
            if columns:
                shape = [columns, size / columns]
            else:
                shape = size
            # IMPORTANT, iotk uses C-style row array storage convention
            retval = retval.reshape(shape, order='C')
        return retval
    else:
        return None

def convert_attr(attr):
    attr_dict = {}
    for string in attr.split():
        key, value = string.split('=')
        value = value.strip('"')
        # Convert Numbers
        try:
            value = ast.literal_eval(value)
        # Check if iotk boolean, otherwise assume it's a string
        except (ValueError, SyntaxError):
            if value == 'T':
                value = True
            elif value == 'F':
                value = False
        attr_dict.update({key: value})
    return attr_dict


class ScanError(Exception):
    def __init__(self, message):
        super(Exception, self).__init__(message)
