
from . import funcs

class readIOTK:
    """ """
    def __init__(self, filename, strict=False, binary=False, raw=False):
        # Input variables
        self.filename = filename
        self.strict = strict
        self._binary = binary
        self._raw = raw

        # Stack variables
        self.stack = []
        self.attrs = []

    @property
    def curattr(self):
        ''' Current Attribute '''
        try:
            return self.attrs[-1]
        except IndexError:
            return None

    def open_read(self):
        ''' IOTK Command '''
        self.iunit = funcs.free_unit()
        temp, attr, self.root = funcs.open_read(self.filename, self.iunit, self._binary, self._raw)

    def close_read(self):
        ''' IOTK Command '''
        funcs.close_read(self.iunit)

    def scan_begin(self, name):
        ''' IOTK Command '''
        found, attr = funcs.scan_begin(self.iunit, name)
        if found:
            # Update the stacks
            self.stack.append(name)
            self.attrs.append(attr)
            return True
        else:
            if self.strict:
                raise TagNotFoundError(name)
            else:
                return False

    def scan_end(self):
        ''' IOTK Commands '''
        try:
            funcs.scan_end(self.iunit, self.stack.pop())
            self.attrs.pop()
        except IndexError:
            pass # Beginning of the stacks

    def scan_empty(self, name):
        ''' IOTK Command '''
        found, attr = funcs.scan_empty(self.iunit, name)
        if found:
            return attr
        else:
            return False

    def scan_dat(self, name, convert=True):
        ''' IOTK Command '''
        return funcs.scan_dat(self.iunit, name, convert)

    def __enter__(self):
        ''' Open the main file (Context Manager) '''
        self.open_read()
        return self

    def __exit__(self, etype, eval, etrace):
        ''' Close the main file (Context Manager) '''
        self.close_read()
        return False


class writeIOTK:
    pass


class TagNotFoundError(Exception):
    def __init__(self, tag):
        super().__init__('Tag {} not found'.format(tag))
