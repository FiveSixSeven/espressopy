import os
import glob
import shutil
from subprocess import Popen, PIPE, getstatusoutput
from . import parsing

class Calculation:
    """
    Quantum Espresso Calculation
    """

    # executable type (exetype) and actual execution command (exe)
    exe_mapping = {
        'pw.x' : 'PWSCF',
        'cp.x' : 'CP',
        'pp.x' : 'PP'
    }

    def __init__(self, invalue=None, infromfile=False, exepath=None, precmd=None, postcmd=None, **kwargs):
        if invalue:
            self.set_infile(invalue, infromfile, **kwargs)
        if exepath:
            self.set_exe(exepath, precmd, postcmd)

    def set_infile(self, invalue=None, infromfile=False, **kwargs):
        self.infile = parsing.iofiles.InputFile(invalue=invalue,
                                                fromfile=infromfile,
                                                **kwargs)

    def set_exe(self, exepath=None, precmd=None, postcmd=None):
        '''
        Set the executable command

        If a parameter is not supplied, and was previously set, set_exe
        will use the previous value
        '''
        try:
            # Redefine object variables from method parameters
            # Note: If corresponding method parameter is not found
            # use the previously set value.
            for attr in ('exepath', 'precmd', 'postcmd'):
                # Initially set to empty quotes
                if not hasattr(self, attr):
                    setattr(self, attr, '')

                # If the value is non-empty, change it
                value = locals()[attr]
                if value:
                    setattr(self, attr, value)

            # Determine the type of the calculation
            try:
                self.exetype = self.__class__.exe_mapping[os.path.basename(self.exepath)]
            except KeyError as ierr:
                raise CalcError('Unknown Quantum-Espresso executable: {}'.format(ierr)) from None
            if self.precmd:
                # Usually for parallel execution
                self.exe = ' '.join([self.precmd, self.exepath, self.postcmd])
            else:
                # Serial execution, always use absolute path
                self.exe = ' '.join([os.path.abspath(self.exepath), self.postcmd])

        except AttributeError as ierr:
            print(ierr)

    def run(self, writeout=None, clean=False):
        '''
        Run the calculation

        infile and exe must be deifned!
        '''
        if not hasattr(self, 'infile'):
            raise CalcError('infile must be set with set_infile before command can be executed')
        if not hasattr(self, 'exe') or not self.exe:
            raise CalcError('exe must be set with set_exe before command can be executed')

        if clean:
            orgdir = set(os.listdir(os.getcwd()))

        # If outfile is present we will write to the output file directly
        if writeout:
            call = Popen(self.exe.strip().split(), stdin=PIPE, stdout=open(writeout, 'w'), stderr=PIPE)
        else:
            call = Popen(self.exe.strip().split(), stdin=PIPE, stdout=PIPE, stderr=PIPE)

        stdout, stderr = call.communicate(self.infile.write().encode('utf-8'))
        if stderr:
            raise CalcRunError(stdout.decode('ascii'), stderr.decode('ascii'))

        # If outfile is present we will read in the file
        if writeout:
            self.outfile = parsing.iofiles.OutputFile(outvalue=writeout, fromfile=True)
        else:
            self.outfile = parsing.iofiles.OutputFile(stdout.decode('ascii'))

        # Remove, any files/directories generated by command (clean = True)
        if clean:
            curdir = set(os.listdir(os.getcwd()))
            for item in curdir.difference(orgdir):
                if os.path.isdir(item):
                    shutil.rmtree(item)
                else:
                    os.remove(item)

    def copy_to_backup(self, backup, source=None):
        ''' Backup all files in outdir to backup '''
        assert source != backup, "Cannot backup to same location"
        if not source:
            source = self.infile.namelist_get('CONTROL', 'outdir')
        sys_rsync(source + '/*', backup)

    def save_to_storage(self, storage, source=None, append='', ignore_ext=None):
        ''' Move files in outdir to storage'''
        assert source != storage, "storage and outdir cannot be location"

        if not source:
            source = self.infile.namelist_get('CONTROL', 'outdir')

        children = glob.glob('{}/*'.format(source))
        # Loop through all children in tempdir
        for child in children:
            basechild = os.path.basename(child)
            newchild = os.path.join(storage, basechild)
            try:
                if ignore_ext:
                    for ext in ignore_ext:
                        if basechild.endswith(ext):
                            raise NameError
            except NameError:
                continue
            if basechild.endswith('.save'):
                sys_cp(child, newchild + append)
            else:
                sys_mv(child, newchild + append)

# START Production Runs =============================

class ProdRun:

    def __init__(self):
        self._calcs = []

    def addcalc(self, calc, fileroot, until_match=None, until_func=None, until_num=None,
              tempdir=None, storage=None, store_ignore=['wfc', 'igk'], store_fmt='05d', backup=None):
        setattr

    def check_complete(self):
        pass


# def autorun(calc, fileroot, until_match=None, until_func=None, until_num=None,
#              tempdir=None, storage=None, store_ignore=['wfc', 'igk'], store_fmt='05d', backup=None):
#     ''' Automatic Calculations '''
#     inext = '.in'
#     outext = '.out'
#
#     if not isinstance(calc, Calculation):
#         raise CalcError('calc must be and instance of EspressoPy.runtime.Calculation')
#
#     if (tempdir or backup) and not tempdir:
#         raise CalcError('Cannot use backup of storage without tempdir')
#
#     if until_match is None and until_func is None and until_num is None:
#         until_num = 1
#
#     # Check all directories (make them if not found)
#     sys_mkdirs(backup, tempdir, storage)
#     sys_clean_dirs(backup)
#
#     # Change outdir to parameter tempdir
#     if tempdir:
#         calc.infile.namelist_set('CONTROL', 'outdir', tempdir)
#         print('Setting outdir directory to {}'.format(tempdir))
#
#     inputfile = fileroot + inext
#     print('Input file: {}'.format(inputfile))
#     calc.print_files(input_name=inputfile)
#
#     ncount = 1
#     while True:
#
#         # Run the actual calculation
#         print("\nCalculation {} ".format(ncount))
#
#         # Backup up temp directory, if it is there and has contents
#         if backup and os.path.isdir(backup) and os.listdir(backup):
#             sys_rsync(tempdir + '/*', backup)
#
#         # Run the calculation
#         outputfile = fileroot + '{}{:{}}'.format(outext, ncount, store_fmt)
#         print('Output File: {}'.format(outputfile))
#         print("Running {}".format(calc.exe))
#         calc.run(writeout=outputfile)
#
#         # Move or copy items to storage
#         if storage:
#             children = glob.glob('{}/*'.format(tempdir))
#             # Loop through all children in tempdir
#             for child in children:
#                 basechild = os.path.basename(child)
#                 newchild = os.path.join(storage, basechild)
#                 try:
#                     for ext in store_ignore:
#                         if basechild.endswith(ext):
#                             raise NameError
#                 except NameError:
#                     break
#                 if basechild.endswith('.save'):
#                     sys_mv(child, newchild + '{:{}}'.format(ncount, store_fmt))
#                 else:
#                     sys_cp(child, newchild + '{:{}}'.format(ncount, store_fmt))
#
#         # Check ending conditions
#         if until_match:
#             if calc.outfile.match(until_match):
#                 msg = "Match '{}' found".format(until_match)
#                 break
#         elif until_func:
#             if until_func(calc, ncount):
#                 msg = "Function '{}' return True".format(until_func)
#                 break
#         elif until_num:
#             if ncount >= until_num:
#                 msg = "Reached limit {}".format(until_num)
#                 break
#
#         ncount += 1
#
#     print("Calculation Complete: {}".format(msg))
# END Production Runs =============================

# START Helper Functions =============================

def sys_cp(src, dest):
    ''' Copy a file using system cp '''
    if os.path.isfile(dest) or os.path.isdir(dest):
        raise CalcError('Destination already exists "{}", cannot copy'.format(dest))
    stderr, stdout = getstatusoutput('cp -rv  {} {}'.format(src, dest))
    if stderr:
        raise CalcError('Error: {}'.format(stderr))
    print("Copying {} to {}".format(src, dest))

def sys_mv(src, dest):
    ''' Move a file using system mv '''
    if os.path.isfile(dest) or os.path.isdir(dest):
        raise CalcError('Destination already exists "{}", cannot move'.format(dest))
    stderr, stdout = getstatusoutput('mv -v {} {}'.format(src, dest))
    if stderr:
        raise CalcError('Error: {}'.format(stdout))
    print("Moving {} to {}".format(src, dest))

def sys_rsync(src, dest):
    ''' sync files (recursively) using system rsync '''
    print("Rsyncing: {} to {}".format(src, dest))
    stderr, stdout = getstatusoutput('rsync -r {} {}'.format(src, dest))
    if stderr:
        raise CalcError('Error: {}'.format(stderr))

def sys_mkdirs(*dirs):
    ''' Check and create directories '''
    for dirname in dirs:
        if dirname and not os.path.isdir(dirname):
            print('Directory: {} created'.format(dirname))
            stderr, stdout = getstatusoutput('mkdir -p {}'.format(dirname))
            if stderr:
                raise CalcError('Error: {}'.format(stderr))
        elif dirname:
            print("Directory: {} found".format(dirname))


def sys_grep(pattern, files):
    ''' Use system grep to search for a pattern and return matches'''
    stderr, stdout = getstatusoutput('grep -e "{}" {}'.format(pattern, files))
    if stdout:
        print("Found {} in {}".format(pattern, files))
    return stdout

# END Helper Functions =============================

# START Exceptions =============================

class CalcError(Exception):
    pass

class CalcRunError(Exception):
    ''' Access to stdout and stderr '''
    def __init__(self, stdout, stderr):
        super().__init__("Calculation Crashed, use exception attributes stdout, stderr")
        self.stdout = stdout
        self.stderr = stderr

class CalcComplete(Exception):
    pass
# END Exceptions =============================
