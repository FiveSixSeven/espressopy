#! /usr/bin/env python
#PBS
#PBS
#PBS
#PBS
#PBS

import sys
import os
import glob
import re
sys.path.append('/home/charles/Devel/')
import EspressoPy
from EspressoPy.runtime import Calculation

# QUANTUM-ESPRESSO CP MD Calculation
# 1) resarted MD until num1

# =-----------------------------------------= #
#   User Input
# =-----------------------------------------= #
template1 = 'h2o-mol1.template'
num1 = 13

storage1 = './store/MD'
precmd1 = 'mpirun -n 4'
exe1 = '/home/charles/Programming/Software-Packages/espresso-5.1/bin/cp.x'

backup = './backup'
outdir = './tempdir'
workdir = '.'
# =-----------------------------------------= #

# Directories

FMT = '05d'

EspressoPy.runtime.sys_mkdirs(storage1, backup, outdir)
os.chdir(workdir)

# ==== START Calc 1 ==== # job information
name = 'MD'
inroot = 'md.in'
outroot = 'md.out'
calc = Calculation(invalue=template1, infromfile=True, precmd=precmd1, exepath=exe1)
print('\nCalculation: {}'.format(name))

# Determine if it should run
files = sorted(glob.glob(outroot + '*'))
if files:
    lastfile = files[-1]
    lastnum = re.sub(outroot + '.', '', lastfile)
    print('Last file: {}'.format(lastfile))
    run = int(lastnum) < num1
else:
    lastnum = 0
    run = True

# Run the calculation
if run:

    calc.infile.namelist_set('CONTROL', 'outdir', outdir)
    calc.infile.write(filename=inroot)

    nextnum = '{:{}}'.format(int(lastnum) + 1, FMT)
    nextfile = '{}{}'.format(outroot, nextnum)

    calc.copy_to_backup(backup)
    print("Running {} < {} > {}".format(calc.exe, inroot, nextfile))
    calc.run(writeout=nextfile)
    calc.save_to_storage(storage1, append='.' + nextnum, ignore_ext=['igk'])

    print("{} Done!".format(name))
    sys.exit(0)
else:
    print('Calculation limit reached ({}) Complete!'.format(num1))
# ==== END Calc 1 ====
