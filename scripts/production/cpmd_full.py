#! /usr/bin/env python
#PBS
#PBS
#PBS
#PBS
#PBS

import sys
import os
import glob
import re
sys.path.append('/home/charles/Devel/')
import EspressoPy
from EspressoPy.runtime import Calculation

# QUANTUM-ESPRESSO CP GS-MD Calculation
# 1) from_scratch groudstate calculation
# 2) restarted groudstate until "MAIN:
# 3) resarted MD until num3

# =-----------------------------------------= #
#   User Input
# =-----------------------------------------= #
template1 = 'h2o-mol1.template'
template2 = 'h2o-mol2.template'
template3 = 'h2o-mol3.template'
num3 = 3

storage1 = storage2 = './store/GS'
storage3 = './store/MD'

precmd1 = precmd2 = precmd3 = 'mpirun -n 4'
exe1 = exe2 = exe3 = '/home/charles/Programming/Software-Packages/espresso-5.1/bin/cp.x'

backup = './backup'
outdir = './tempdir'
workdir = '.'
# =-----------------------------------------= #

FMT = '05d'

EspressoPy.runtime.sys_mkdirs(storage1, storage2, storage3, backup, outdir)
os.chdir(workdir)

# ==== START Calc 1 ====
name = 'GS Inital'
inroot = 'gs.in'
outroot = 'gs.out'
calc = Calculation(invalue=template1, infromfile=True, precmd=precmd1, exepath=exe1)
print('\nCalculation: {}'.format(name))

# Determine if it should run
files = sorted(glob.glob(outroot + '*'))
if files:
    run = False
else:
    lastnum = 0
    run = True

# Run the calculation
if run:

    calc.infile.namelist_set('CONTROL', 'outdir', outdir)
    calc.infile.write(filename=inroot)

    nextnum = '{:{}}'.format(int(lastnum) + 1, FMT)
    nextfile = '{}{}'.format(outroot, nextnum)

    print("Running {} < {} > {}".format(calc.exe, inroot, nextfile))
    calc.run(writeout=nextfile)
    calc.save_to_storage(storage1, append='.' + nextnum, ignore_ext=['igk'])

    print("{} Done!".format(name))
    sys.exit(0)
else:
    print('Calculation Complete!')
# ==== END Calc 1 ====


# ==== START Calc 2 ====
# job information
name = 'GS Restart'
inroot = 'gs_rst.in'
outroot = 'gs.out'
calc = Calculation(invalue=template2, infromfile=True, precmd=precmd2, exepath=exe2)
print('\nCalculation: {}'.format(name))

# Determine if it should run
files = sorted(glob.glob(outroot + '*'))
if files:
    lastfile = files[-1]
    lastnum = re.sub(outroot + '.', '', lastfile)
    print('Last file: {}'.format(lastfile))
    # ADD Condition to run
    run = not EspressoPy.runtime.sys_grep('MAIN:', lastfile)

# Run the calculation
if run:

    calc.infile.namelist_set('CONTROL', 'outdir', outdir)
    calc.infile.write(filename=inroot)

    nextnum = '{:{}}'.format(int(lastnum) + 1, FMT)
    nextfile = '{}{}'.format(outroot, nextnum)

    calc.copy_to_backup(backup)
    print("Running {} < {} > {}".format(calc.exe, inroot, nextfile))
    calc.run(writeout=nextfile)
    calc.save_to_storage(storage2, append='.' + nextnum, ignore_ext=['igk'])

    print("{} Done!".format(name))
    sys.exit(0)
else:
    print('Calculation Complete!')
# ==== END Calc 2 ====


# ==== START Calc 3 ====
# job information
name = 'MD'
inroot = 'md.in'
outroot = 'md.out'
calc = Calculation(invalue=template3, infromfile=True, precmd=precmd3, exepath=exe3)
print('\nCalculation: {}'.format(name))

# Determine if it should run
files = sorted(glob.glob(outroot + '*'))
if files:
    lastfile = files[-1]
    lastnum = re.sub(outroot + '.', '', lastfile)
    print('Last file: {}'.format(lastfile))
    run = int(lastnum) < num3
else:
    lastnum = 0
    run = True

# Run the calculation
if run:

    calc.infile.namelist_set('CONTROL', 'outdir', outdir)
    calc.infile.write(filename=inroot)

    nextnum = '{:{}}'.format(int(lastnum) + 1, FMT)
    nextfile = '{}{}'.format(outroot, nextnum)

    calc.copy_to_backup(backup)
    print("Running {} < {} > {}".format(calc.exe, inroot, nextfile))
    calc.run(writeout=nextfile)
    calc.save_to_storage(storage3, append='.' + nextnum, ignore_ext=['wfc', 'igk'])

    print("{} Done!".format(name))
    sys.exit(0)
else:
    print('Calculation limit reached ({}) Complete!'.format(num3))
# ==== END Calc 3 ====
