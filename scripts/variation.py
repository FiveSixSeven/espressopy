#! /usr/bin/env python

import sys
import os
import timeit
import argparse
import configparser
try:
    filedir = os.path.dirname(os.path.realpath(__file__))
    sys.path.append(os.path.join(filedir, os.pardir, os.pardir))
    import EspressoPy
except ImportError as ierr:
    print(ierr)
    sys.exit(1)

_SECTION = 'setup'
_NAMELIST = 'namelist'
_CARD = 'card'

# strip quotes
qstrip = lambda string : string.strip("\"'")


class InputSetup:
    '''
    Read Configuration File and Data Files (iterator)
    '''

    def __init__(self, configfile):

        self.parser = configparser.ConfigParser()
        if not self.parser.read(configfile):
            raise VariationError()

        try:
            self._read_parser()
        except (configparser.NoOptionError, configparser.NoSectionError) as ierr:
            raise VariationError(ierr)

    def _read_parser(self):
        '''
        Read the config file
        - template
        - type_select (namelist or card)
        - type_name (namelist or card name)
        - datafile
        - exepath
        - variable (Namelist only)
        - data_length (Cards only)
        '''
        self.template = qstrip(self.parser.get(_SECTION, 'template'))
        self.type_select = qstrip(self.parser.get(_SECTION, 'type_select'))
        self.type_name = qstrip(self.parser.get(_SECTION, 'type_name'))
        self.datafile = qstrip(self.parser.get(_SECTION, 'datafile'))
        self.exepath = qstrip(self.parser.get(_SECTION, 'exepath'))
        self.variable = None
        self.data_length = None

        # calls the correct steps function (_namelist_steps, or _card_steps)
        self.steps = None

        # selection: namelist
        if self.type_select == _NAMELIST:
            self.variable = qstrip(self.parser.get(_SECTION, 'variable'))
            self.steps = list(self._namelist_steps())
        # selection: card
        elif self.type_select == _CARD:
            self.data_length = int(qstrip(self.parser.get(_SECTION, 'data_length')))
            self.steps = list(self._card_steps())
        else:
            raise VariationError('unknown value for type_select: {}'.format(self.type_select))

    def _namelist_steps(self):
        '''
        Generator for all the values of the selected namelist variable
        Yields individual values (One per line)
        '''
        with open(self.datafile, 'r') as fdata:
            while True:
                line = fdata.readline().strip()
                if line:
                    yield line
                else:
                    break

    def _card_steps(self):
        '''
        Generator for all the values of the selected card
        Yields lists using self.data_length
        '''
        with open(self.datafile, 'r') as fdata:
            temp = []
            while True:
                line = fdata.readline().strip()
                if line:
                    temp.append(line)
                    if len(temp) == self.data_length:
                        # Yield data list, setup for next cycle
                        yield temp
                        temp = []
                else:
                    break


class VarCalculation(EspressoPy.runtime.Calculation):
    '''
    Variation Calculation object (subclass of EspressoPy.runtime.Calculation):

    Original initialization parameters:

        @inputfile
        @fromfile
        @exepath
        @prefix
        @postfix
        @kwargs

    This Subclass adds the following, which are used in Variation:

        @type_select : selects the type of data to be varied ('namelist', or 'card')
        @type_name : selects the name of namelist or card (ex: 'ATOMIC_POSITIONS', 'CONTORL', ...)
        @variable : if type_select is 'namelist' this elects the variable within the namelist
        @update : A pointer to either self._namelist_update or self._card_update
        @clean : boolean, clean all generated files form each Calculation
    '''
    def __init__(self, type_select, type_name, *args, variable=None, clean=True, **kwargs):
        self.type_select = type_select
        self.type_name = type_name
        self.variable = variable
        self.update = None
        self.clean = clean
        super().__init__(*args, **kwargs)

        # Set pointer to self.update (either _namelist_update, _card_update)
        if self.type_select == _NAMELIST:
            self.update = self._namelist_update
            if not self.variable:
                raise VariationError('Name of variable not supplied to VarCalculation')
        elif self.type_select == _CARD:
            self.update = self._card_update
        else:
            raise VariationError('unknown value for type_select: {}'.format(self.type_select))

    def _namelist_update(self, value):
        '''Update selected namelists'''
        self.infile.namelist_set(self.type_name, self.variable, value)

    def _card_update(self, value):
        self.infile.card_set(self.type_name, value)

    def run(self, *args, **kwargs):
        '''Wrapper for run from the superclass (May be removed)'''
        super().run(*args, clean=self.clean, **kwargs)


class Variation(object):
    '''
        Setup the Variation Command Object

        Requires a VarCalculation object that has been setup for the variation of
        a namelist of a card.

        Initialization parameters:
            @varcal : VarCalculation obj (subclass of EspressoPy.runtime.Calculation, see above)
            @varsteps : List of values for which the VarCalculation object will be varied
            @outfile : desired name of final output file
            @printall : boolean, print a copy of all input and output files

    '''
    def __init__(self, varcalc, varsteps, outfile, printall):

        self.calc = varcalc
        self.varsteps = varsteps
        self.outfile = outfile
        self.printall = printall

    def variation_loop(self):
        '''Main Variation loop'''
        try:
            self._variation_init()

            # stop+step so that we actually hit the stop
            print('Main Variation loop:')
            for index, value in enumerate(self.varsteps):

                # Update the data
                self.calc.update(value)

                # Run Quantum Espresso
                start_time = timeit.default_timer()
                self.calc.run()
                end_time = timeit.default_timer()

                # Update function for the variation loop
                self._variation_update(index, value, end_time - start_time)

                # Printout all files
                if self.printall:
                    self._printall_files(self.calc, index)

                # Display to the console
                self._display(index, value)

        except EspressoPy.runtime.CalcRunError as ierr:
            self._crash_report(ierr)

    def _variation_init(self):
        self.varvals = []
        self.totengs = []
        self.times = []

    def _variation_update(self, index, value, time):
        self.varvals.append(value)
        self.times.append(time)
        if self.calc.outfile.complete:
            self.totengs.append(self.calc.outfile.toteng)
        else:
            self.totengs.append('')

    def _printall_files(self, calc, index):
        # Printout all files for a particular calculation
        # Set root input/output name
        prefix = calc.infile.namelist_get('CONTROL', 'prefix')
        if prefix:
            printall_root = prefix
        else:
            printall_root = calc.exetype

        calc.print_files(
            input_name='{}.in{}'.format(printall_root, index + 1),
            output_name='{}.out{}'.format(printall_root, index + 1)
        )

    def _display(self, index, value):
        nat = int(self.calc.infile.namelist_get('SYSTEM', 'nat'))

        # Display to console
        if self.calc.type_select == _NAMELIST:
            print('{:4d}) {:<20}'.format(index + 1, value), end='')
        elif self.calc.type_select == _CARD:
            print('{:4d}) '.format(index + 1), end='')
        print('     {:12.7f} {}'.format(self.totengs[-1], self.calc.outfile.toteng_units), end='')
        print('     {:12.7f} {}/atom      in {:5.2f} s'.format(self.totengs[-1] / nat,
                                                 self.calc.outfile.toteng_units, self.times[-1]))

    def _crash_report(self, ierr):
        print('\n\n==================================== \n'
              'Calculation Crashed:\n'
              'Check: CRASH.in, CRASH.out, CRASH'
              '\n==================================== \n')
        print(ierr.stdout)
        print(ierr.stderr)
        with open('CRASH.in', 'w') as f:
            print(self.calc.infile.write(), file=f)
        with open('CRASH.out', 'w') as f:
            print(ierr.stdout, ierr.stderr, file=f)
        sys.exit(1)

    def write_data(self):
        print("\nWriting output: {}".format(self.outfile))
        nat = int(self.calc.infile.namelist_get('SYSTEM', 'nat'))
        with open(self.outfile, 'w') as f:
            ncount = 0
            for varval, eng, time in zip(self.varvals, self.totengs, self.times):
                ncount += 1
                if self.calc.type_select == _NAMELIST:
                    print('{:7d}  {:20} {:15.7f} {:15.7f} {:15.3f}'.format(ncount,
                                                        varval, eng, eng / nat, time), file=f)
                elif self.calc.type_select == _CARD:
                    print('{:7d}  {:15.7f} {:15.7f} {:15.3f}'.format(ncount,
                                                        eng, eng / nat, time), file=f)
                    print('\n'.join(varval), end='\n\n', file=f)


class VariationError(Exception):
    pass

# =-----------------------------------------------------------------------------= #
#  Script execution functions
# =-----------------------------------------------------------------------------= #
def parse_argv(argv):

    welcome = "Quantum Espresso variation Study Script"
    parser = argparse.ArgumentParser(
        # usage='%(prog)s [options] positional',
        description=welcome,
        formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        'configfile',
        action='store',
        type=str,
        help='Configuration file with: \n[setup]'
             '\ntemplate = <template file>\n'
             'type_select = <\'namelist\' or \'card\'>\n'
             'type_name = <name of type_select>\n'
             'variable = <variable name (namelist only)>\n'
             'data_length = <length of each record (card only)>\n'
             'datafile = <location of variation data\n'
             'exepath = <path for executable>\n'
    )
    parser.add_argument(
        '--outfile',
        action='store',
        type=str,
        default='variation.dat',
        help='File for energy and variation data'
    )
    parser.add_argument(
        '--prefix',
        action='store',
        type=str,
        default='',
        help='String to precede the executable (example: \'mpirun -np 4\')'
    )
    parser.add_argument(
        '--postfix',
        action='store',
        type=str,
        default='',
        help='Command line arguments to the executable'
    )
    parser.add_argument(
        '--clean',
        action='store_true',
        default=False,
        help='Clean working directory of all generated files'
    )
    parser.add_argument(
        '--printall',
        action='store_true',
        default=False,
        help='Print all input and output files as prefix.in<NUM>/prefix.out<NUM>, set clean = False'
    )
    print()
    args = parser.parse_args()

    return args

def welcome_print(type_select, type_name, template, exe, printall,
                                                clean, outfile, variable=None):
    print("\nQuantum Espresso Variation Study\n")
    if type_select == _NAMELIST:
        print("Varying: {}, {}".format(type_name, variable))
    elif type_select == _CARD:
        print("Varying: {}".format(type_name))
    print("Template File: {}".format(template))
    print("Executable: {}".format(exe))
    print("Printall: {}".format(printall))
    print("Clean: {}".format(clean))
    print("Outputfile: {}".format(outfile))
    print()


if __name__ == '__main__':
    args = parse_argv(sys.argv[1:])
    config = InputSetup(args.configfile)
    varcalc = VarCalculation(
        config.type_select,
        config.type_name,
        variable=config.variable,
        clean=args.clean,
        inputfile=config.template,
        fromfile=True,
        exepath=config.exepath,
        prefix=args.prefix,
        postfix=args.postfix
    )
    varcmd = Variation(varcalc, config.steps, args.outfile, args.printall)
    welcome_print(
        config.type_select,
        config.type_name,
        config.template,
        varcalc.exe,
        args.printall,
        args.clean,
        args.outfile,
        config.variable
    )
    varcmd.variation_loop()
    varcmd.write_data()
