import sys
import collections
import re
import numpy as np

BOHR_2_ANGSTROM = 0.52918

def main(posfile, celfile, atoms):

    # Check file types
    p = re.compile('\.pos')
    if p.search(posfile):
        outfile = p.sub('.axsf', posfile)
    else:
        raise IOError('file is not .pos type')

    if not re.search('\.cel', celfile):
        raise IOError('file is not .cel type')

    # Total atoms
    nat = sum(atoms.values())

    # Positions and cells list
    positions = []
    cells = []

    # Atom info
    print('Atom Infomation:')
    for key, value in atoms.items():
        print("Atom: {}, Number: {}".format(key, value))

    # Read in information
    with open(posfile) as fpos, open(celfile) as fcel:

        # Get the positions
        print('Reading postion file "{}" ... '.format(posfile), end="")
        slines = (line.split() for line in fpos)
        for sline in slines:
            # <step time> line
            if len(sline) == 2:
                positions.append([])
                while len(positions[-1]) < nat:
                    positions[-1].append([float(i) for i in next(slines)])
            else:
                raise IOError('Unexpected file format')
        print("Done")

        # Get the cells
        print('Reading cells file "{}" ... '.format(celfile), end="")
        slines = (line.split() for line in fcel)
        for sline in slines:
            # <step time> line
            if len(sline) == 2:
                cells.append([])
                for i in range(3):
                    cells[-1].append([float(i) for i in next(slines)])
        print("Done")

        if len(positions) != len(cells):
            raise IOError('*.pos and *.cel files have different lengths')
        else:
            print("Number of position frames: {}".format(len(positions)))
            print("Number of cell frames: {}".format(len(cells)))

    # Write the information
    print('Writing output file "{}" ... '.format(outfile), end="")
    with open(outfile, 'w') as fout:
        print('ANIMSTEPS', len(positions), file=fout)
        print('CRYSTAL', file=fout)
        for step, data in enumerate(zip(positions, cells)):
            position, cell = data

            # Print PRIMVEC
            print("PRIMVEC", step + 1, file=fout)
            for vec in cell:
                vec = [i * BOHR_2_ANGSTROM for i in vec]
                print("{:8.4f} {:8.4f} {:8.4f}".format(*vec), file=fout)

            # Print CONVVEC
            print("CONVVEC", step + 1, file=fout)
            for vec in cell:
                vec = [i * BOHR_2_ANGSTROM for i in vec]
                print("{:8.4f} {:8.4f} {:8.4f}".format(*vec), file=fout)

            # Print PRIMCOORD
            print("PRIMCOORD", step + 1, file=fout)
            print(nat, 1, file=fout)
            atoms_info = (item for item in atoms.items())
            acount = 0
            label, num = next(atoms_info)
            for vec in position:
                vec = [i * BOHR_2_ANGSTROM for i in vec]
                if acount == num:
                    acount = 0
                    label, num = next(atoms_info)
                print("{}  {:10.6f} {:10.6f} {:10.6f}".format(label, *vec), file=fout)
                acount += 1
    print('Done')


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Usage: axsf.py posfile celfile atom1=num1 atom2=num2 ...")
        sys.exit(1)
    atoms = collections.OrderedDict()
    for element in sys.argv[3:]:
        key, value = element.split('=')
        atoms.update({key: int(value)})
    main(sys.argv[1], sys.argv[2], atoms)
