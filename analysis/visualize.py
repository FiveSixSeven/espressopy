import numpy as np
from ..physics import atoms
#from ..physics import vectors
from ..physics import grids

class ExternalViewer:
    def __init__(self, atoms, vecs, isosurface=None, origin=[0.0, 0.0, 0.0]):
        if not isinstance(atoms, atoms.AtomPositions):
            raise VisualizeError('atoms is required to be a Atoms object')
        # if not isinstance(vecs, vectors.latvecs):
        #    raise InputError('vecs is required to be a Atoms object')
        if isosurface and not isinstance(isosurface, grids.Isosurface):
            raise VisualizeError('isosurface is required to be an Isosurface object')

        self.atoms = atoms
        self.vecs = vecs
        self.isosurface = isosurface
        self.origin = origin

    def write_xsf(self, filename):
        # self.vecs.conver('angstrom')
        self.atoms.convert('angstrom')
        with open(filename, 'w') as f:
            print('CRYSTAL\nPRIMVEC', file=f)
            for vec in self.vecs:
                print('   '.join(['{:10.5f}'.format(i) for i in vec]), file=f)
            print('PRIMCOORD', file=f)
            print('  {} 1'.format(self.atoms.total), file=f)
            for atype, at in self.atoms.items():
                for atom in atoms:
                    print(atype, '   '.join(['{:10.5f}'.format(i) for i in atom]), file=f)

        # Check for isosruface
        # HACK: We have to close and then reopen the file with a 'ba' mode for np.savetxt
        if self.isosurface:
            with open(filename, 'a') as f:
                print('BEGIN_BLOCK_DATAGRID_3D\n3D_PWSCF\nDATAGRID_3D_UNKNOWN', file=f)
                print(' {} {} {}'.format(
                    len(self.isosurface.xaxis),
                    len(self.isosurface.yaxis),
                    len(self.isosurface.zaxis)
                ), file=f)

                print(' '.join(['{:10.5}'.format(i) for i in self.origin]), file=f)
                for vec in self.vecs:
                    print('   '.join(['{:10.5f}'.format(i) for i in vec]), file=f)

            with open(filename, 'ba') as f:
                np.savetxt(f, self.isosurface.reshape())

            with open(filename, 'a') as f:
                print('END_DATAGRID_3D\nEND_BLOCK_DATAGRID_3D', file=f)

    def write_xyz(self):
        pass


class VisualizeError(Exception):
    pass
