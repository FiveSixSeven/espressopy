import numpy as np

class Isosurface(np.ndarray):
    def __new__(cls, data, *args, **kwargs):
        obj = np.ndarray.__new__(cls, np.shape(data), dtype=float, buffer=data)
        return obj

    def __init__(self, data, xaxis, yaxis, zaxis):
        self.xaxis = np.array(xaxis)
        self.yaxis = np.array(yaxis)
        self.zaxis = np.array(zaxis)

        if len(self.shape) != 3:
            raise IsosurfaceError('Scalar data must be 3D array')
        if set(self.shape) != set([len(self.xaxis), len(self.yaxis), len(self.zaxis)]):
            raise IsosurfaceError('Size of the scalar data and axes does not match')

    def reshape_one_col(self, order='F'):
        length = len(self.xaxis) * len(self.yaxis) * len(self.zaxis)
        return np.reshape(self, length, order=order)

class IsosurfaceError(Exception):
    pass
