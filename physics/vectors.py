import numpy as np

class Vecs(np.ndarray):
    def __new__(cls, *args, **kwargs):
        data = []
        for arg in args:
            if hasattr(arg, 'split'):
                data.append(arg.split())
            else:
                data.append(arg)
        data = np.array(data)
        print(data)
        obj = np.ndarray.__new__(cls, np.shape(data), dtype=float, buffer=data)
        return obj

class LatticeVecs(Vecs):
    def __init__(self, *args, units='bohr', **kwargs):
        self.units = units
