import collections
import numpy as np
# from .convert import distance_to_meters, energy_to_joules

ANG_2_BOHR = 1.889725989
BOHR_2_ANG = 1 / ANG_2_BOHR

class _AtomicXYZProperties(np.ndarray):
    def __new__(cls, data, *args, **kwargs):
        data = np.array(data, dtype=float)
        shape = np.shape(data)
        try:
            if len(shape) != 2 or shape[1] != 3:
                raise TypeError
        except (IndexError, TypeError):
            raise AtomsError('_AtomicXYZProps objects must be constructed with '
                             'shape (*, 3) 2D Numpy arrays or lists')
        obj = np.ndarray.__new__(cls, shape, dtype=float, buffer=data)
        return obj


class _AtomPositions(_AtomicXYZProperties):
    def __init__(self, data, *args, units='bohr', **kwargs):
        self.units = units

    def wrap_pbc(self, xlen, ylen, zlen):
        lengths = xlen, ylen, zlen
        return self - np.floor_divide(self.data, lengths) * lengths

    def convert(self, units):
        pass


class _AtomVelocities(_AtomicXYZProperties):
    def __init__(self, data, units='au'):
        self.units = units

    def convert(self, units):
        pass


class _AtomForces(_AtomicXYZProperties):
    def __init__(self, data, units='au'):
        self.units = units

    def convert(self, units):
        pass


class Atoms(object):

    _array_properties = {'positions': _AtomPositions, 'velocities': _AtomVelocities,
                   'forces': _AtomForces}

    def __init__(self, symbol, positions=None, velocities=None, forces=None):
        self.symbol = symbol

        # Set Atomic Positions, Velocities and Forces (None by default)
        for attr, cls in self._array_properties.items():
            arg_value = locals()[attr]
            if arg_value:
                setattr(self, attr, cls(arg_value))
            else:

                setattr(self, attr, None)

    def __setattr__(self, name, value):

        # Check for consistent shapes when array attributes
        if name in self._array_properties.keys():
            # A value of None, is allowed to specify array_attributes.
            # Therefore we skip the check if the value is None
            if value is not None:
                shapes = []
                for attr in self._array_properties.keys():
                    # Do not check the shape of the preexisting attr
                    if name == attr:
                        continue
                    attr_shape = np.shape(getattr(self, attr))
                    if attr_shape:
                        shapes.append(attr_shape)
                if shapes:
                    if len(frozenset(shapes)) > 1:
                        raise AtomsError('Inconsistent array shapes for XYZ properties')
                value = self._array_properties[name](value)

        return super().__setattr__(name, value)


class AtomSpecies(collections.OrderedDict):
    def __init__(self, *species):
        super().__init__()
        if species:
            self._setup_species(species)
        else:
            raise AtomsError('Species symbols must be provided when instancing Atoms')

    def _setup_species(self, species):
        for symbol in species:
            self.update({symbol: Atoms(symbol)})

    @property
    def total(self):
        nat = 0
        # Set to the first non-zero property (consistency is checked in Atoms)
        for specie in self.keys():
            for property in Atoms._array_properties.keys():
                attr_value = getattr(self[specie], property)
                if attr_value is not None:
                    nat += len(attr_value)
                    break
        return nat

    def convert(self, units, property):
        pass

# Exceptions ==========================

class AtomsError(Exception):
    pass
